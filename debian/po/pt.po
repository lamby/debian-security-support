# Translation of debian-security-support's debconf messages to Portuguese
# Copyright (C) 2014 THE debian-security-support'S COPYRIGHT HOLDER
# This file is distributed under the same license as the debian-security-support package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2014 - 2016.
msgid ""
msgstr ""
"Project-Id-Version: debian-security-support 2016.05.11\n"
"Report-Msgid-Bugs-To: debian-security-support@packages.debian.org\n"
"POT-Creation-Date: 2016-05-12 09:42+0200\n"
"PO-Revision-Date: 2016-05-12 21:40+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.4\n"

#. Type: text
#. Description
#: ../debian-security-support.templates:2001
msgid "Ended security support for one or more packages"
msgstr "Suporte de segurança terminado para um ou mais pacotes"

#. Type: text
#. Description
#: ../debian-security-support.templates:2001
msgid ""
"Unfortunately, it has been necessary to end security support for some "
"packages before the end of the regular security maintenance life cycle."
msgstr ""
"Infelizmente, foi necessário terminar o suporte de segurança para alguns "
"pacotes antes do fim do ciclo de vida normal da manutenção de segurança."

#. Type: text
#. Description
#. Type: text
#. Description
#. Type: text
#. Description
#: ../debian-security-support.templates:2001
#: ../debian-security-support.templates:3001
#: ../debian-security-support.templates:4001
msgid "The following packages found on this system are affected by this:"
msgstr "São afectados por isto os seguintes pacotes encontrados neste sistema:"

#. Type: text
#. Description
#: ../debian-security-support.templates:3001
msgid "Limited security support for one or more packages"
msgstr "Suporte de segurança limitado para um ou mais pacotes"

#. Type: text
#. Description
#: ../debian-security-support.templates:3001
msgid ""
"Unfortunately, it has been necessary to limit security support for some "
"packages."
msgstr ""
"Infelizmente, foi necessário limitar o suporte de segurança para alguns "
"pacotes."

#. Type: text
#. Description
#: ../debian-security-support.templates:4001
msgid "Future end of support for one or more packages"
msgstr "Futuro final de suporte para um ou mais pacotes"

#. Type: text
#. Description
#: ../debian-security-support.templates:4001
msgid ""
"Unfortunately, it will be necessary to end security support for some "
"packages before the end of the regular security maintenance life cycle."
msgstr ""
"Infelizmente, irá ser necessário terminar o suporte de segurança para alguns "
"pacotes antes do fim do ciclo de vida normal da manutenção de segurança."
