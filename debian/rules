#!/usr/bin/make -f

NEXT_VERSION_ID=11

DEBIAN_VERSION ?= $(shell cat /etc/debian_version | grep '[0-9.]' | cut -d. -f1)
ifeq (,$(DEBIAN_VERSION))
  DEBIAN_VERSION=$(NEXT_VERSION_ID)
endif

VER=$(shell dpkg-parsechangelog | grep "^Version" | awk '{print $$2}' )

%:
	dh $@

override_dh_clean:
	dh_clean
	-rm man/check-support-status.*1
	-rm check-support-status
	debconf-updatepo
	make -C po clean

override_dh_auto_install:
	dh_auto_install --destdir=debian/tmp
	# compile and install catalogs
	make -C po update-mo
	make -C po install

override_dh_install:
	mkdir -p debian/debian-security-support/usr/share/debian-security-support/
	cp security-support-ended.deb* debian/debian-security-support/usr/share/debian-security-support/
	cp security-support-limited debian/debian-security-support/usr/share/debian-security-support/security-support-limited
	dh_install --fail-missing

override_dh_installman:
	for man in man/check-support-status.txt.* ; do \
		[ -f "$$man" ] || continue ; \
		lang="$$(basename "$$man" | sed -e 's/^check-support-status\.txt\.//')" ; \
		a2x --format=manpage --no-xmllint "$$man" ; \
		mv man/check-support-status.1 "man/check-support-status.$$lang.1" ; \
		sed -i -e "s/\[% VERSION %\]/$(VER)/" "man/check-support-status.$$lang.1" ; \
	done
	a2x --format=manpage --no-xmllint man/check-support-status.txt
	sed -i -e "s/\[% VERSION %\]/$(VER)/" man/check-support-status.1
	# ignore an empty manpage created during build
	-rm debian/debian-security-support/usr/share/man/man1/check-support-status.1p
	dh_installman

override_dh_link:
	# After Closing: #762594, check-security-status reads the debian version
	# specific list file for ended and earlyend. This symlink is aimed to
	# prevent breaking anything relying on the security-support-ended list
	# file.
	dh_link usr/share/debian-security-support/security-support-ended.deb"$(DEBIAN_VERSION)" usr/share/debian-security-support/security-support-ended
